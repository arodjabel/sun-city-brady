var express = require('express');
var app = express();
const path = require('path');

const port = process.env.PORT || 3000;

app.all('/*', function (req, res) {
    res.sendFile(path.join('/build/', req.path), {root: __dirname});
});

app.listen(port);