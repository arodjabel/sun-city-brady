'use strict';

const localArcFns = require('./node/archiver.js');
const filesToCompress = ['app.js', 'package.json'];
const dirsToCompress = ['build'];

localArcFns.methods.runTheArchive(filesToCompress, dirsToCompress);
