import '../shared_libraries/font-awesome-4.7.0/css/font-awesome.css'
import '../shared_libraries/bootstrap-3.3.4-dist/css/bootstrap.css'
import '../shared_libraries/google-fonts/Montserrat/fonts/fonts.css'
import '../shared_libraries/ionicons-2.0.1/css/ionicons.css'
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
