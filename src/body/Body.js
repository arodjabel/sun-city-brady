import React, {Component} from 'react';
import './Body.css';

class Body extends Component {
    render() {
        return (
            <div className='body-section'>
                <div className='row'>
                    <div className='col-xs-12 body-welcome-row'>
                        <span className="body-welcome-row-text">Sun City Brady</span>
                    </div>
                    <div className='col-xs-offset-1 col-xs-4 body-welcome-seperator-line'></div>
                    <div className='col-xs-2 body-welcome-seperator'>
                        <i className="fa fa-handshake-o body-welcome-seperator-icon" aria-hidden="true"></i>
                    </div>
                    <div className='col-xs-4 body-welcome-seperator-line'></div>
                </div>
                <div className='row'>
                    <div className='col-xs-12 body-spacer'>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-12 body-tag-line-row'>
                        <span>Feature, Advantage, Benefit</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Body;