import React, {Component} from 'react';

import './App.css';
import Header from './header/Header';
import Sidebar from './sidebar/Sidebar';
import Body from './body/Body';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Header />
                <Sidebar />
                <Body />
            </div>
        );
    }
}

export default App;
