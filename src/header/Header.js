import React, {Component} from 'react';
import './Header.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};

        // This binding is necessary to make `this` work in the callback
        this.toggleSidebar = this.toggleSidebar.bind(this);
    }

    toggleSidebar() {
        this.setState({isToggleOn: !this.state.isToggleOn});
    }

    render() {
        return (
            <div className='header-section'>
                <i className="ion-ios-arrow-left" onClick={this.toggleSidebar}></i>
            </div>
        )
    }
}

export default Header;