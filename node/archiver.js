'use strict';
const archiver = require('archiver');
const path = require('path');
const fs = require('fs');

let filesToCompress;
let dirsToCompress;

function createArchive(dir) {
    return new Promise((resolve, reject) => {
        var archive,
            output,
            f,
            d,
            finalizeCounter = 0,
            zipName = './Archive.zip';

        output = fs.createWriteStream(dir + '/Archive.zip');
        archive = archiver.create('zip', {});
        archive.pipe(output);

        function finalizeArchive() {
            finalizeCounter++;
            if (finalizeCounter === (filesToCompress.length + dirsToCompress.length)) {
                archive.finalize();
                console.log('created blank Archive.zip');
                resolve();
            }
        }

        for (f = 0; f < filesToCompress.length; f++) {
            archive.file(path.join(dir, filesToCompress[f]), {name: filesToCompress[f]});
            finalizeArchive();
        }

        for (d = 0; d < dirsToCompress.length; d++) {
            archive.directory(path.join(dir, dirsToCompress[d]), path.join('./', dirsToCompress[d]), {name: dirsToCompress[d]});
            finalizeArchive();
        }
    });
}

function deleteArchive(dir, archive) {
    return new Promise(function (resolve, reject) {
        fs.unlink(archive, function (err) {
            if (!err) {
                console.log('deleted Archive.zip');
                createArchive(dir).then(function () {
                    resolve();
                });
            } else {
                console.log('deleting Archive.zip failed');
                resolve();
            }
        });
    });
}

function runTheArchive(_filesToCompress, _dirsToCompress) {
    filesToCompress = _filesToCompress;
    dirsToCompress = _dirsToCompress;

    const dir = path.resolve(path.join(__dirname, '/../'));
    const archive = path.resolve(__dirname, 'Archive.zip');

    //does archive exist?
    function startDeploy() {
        function closeTheFile(fd) {
            fs.close(fd);
        }

        fs.open(archive, 'r', function (err, fd) {
            if (err) {
                if (err.code === 'ENOENT') {
                    console.error('Archive.zip is not available');
                    createArchive(dir).then(function () {
                        closeTheFile(fd);
                    });
                } else {
                    deleteArchive(dir, archive).then(function () {
                        closeTheFile(fd);
                    });
                }
            } else {
                deleteArchive(dir, archive).then(function () {
                    closeTheFile(fd);
                });
            }
        });
    }

    startDeploy();
}

exports.methods = {
    createArchive,
    deleteArchive,
    runTheArchive
};